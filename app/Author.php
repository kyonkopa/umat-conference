<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'authors';
    
    public function categories(){
        return $this->belongsToMany('App\Category', 'author_category', 'author_id', 'category_id');
    }
}
