<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthorsCategory extends Model
{
    protected $table = 'authors_category';
    
    public function author(){
        return $this->belongsTo('App\Author', 'id', 'author_id');
    }
    
    public function category(){
        return $this->hasOne('App\Category', 'id', 'category_id');
    }
}
