<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    
    public function authors(){
        return $this->belongsToMany('App\Author', 'author_category', 'category_id', 'author_id');
    }
}
