<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use App\Paper;
use App\Author;
use App\Http\Requests;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    public function programme(){
        $filename = 'test.pdf';
        $path = storage_path('app/'.$filename);

        return response(file_get_contents($path), 200)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
    }
    
    public function paper($category, $title){
        $filename = $title.'.pdf';
        $path = storage_path('app/'.$category.'/'.$filename);
        
        return response(file_get_contents($path), 200)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
    }
    
    public function delegates(){
        $filename = 'Delegate List.pdf';
        $path = storage_path('app/'.$filename);
        
        return response(file_get_contents($path), 200)
            ->header('Content-Type', 'application/pdf')
            ->header('Content-Disposition', 'inline; filename="'.$filename.'"');
    }
    
    public function search(){      
        $query = Input::get('q');
        return $papers = DB::select("SELECT * FROM `papers` WHERE MATCH(title) AGAINST('{$query}' IN NATURAL LANGUAGE MODE)");
        
//        $authors = DB::select("SELECT * FROM `authors` WHERE MATCH(fullname) AGAINST('{$query}' IN NATURAL LANGUAGE MODE)");
        
//        return $authors;
    } 
}
 