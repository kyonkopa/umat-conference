<?php namespace App\Http\Controllers;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PortalController extends Controller {
	
	public function __construct(){
	}
	
	public function index(){
		$loggedIn = false;
		return view('portal.index', compact('loggedIn'));
	}

	public function register(){
		return view('portal.register');
	}

	public function welcome(){
		return view('portal.welcome');
	}
}
