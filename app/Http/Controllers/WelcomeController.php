<?php namespace App\Http\Controllers;
use App\User;
use App\AuthorsCategory;
use App\Category;
use App\Paper;
use App\Author;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;
	 

class WelcomeController extends Controller {
	
	public function index(){
		$title = 'Home';
		return view('welcome', compact('title'));
	}
	
	public function programme(){
		$title = 'Programme';
		$document = 'programme';
		return view('show', compact('title', 'document'));
	}	
	
	public function paperCategories(){
		$title = 'Conference Papers';
		$categories = Category::all();
		
		return view('papers', compact('title', 'categories'));
	}
	
	public function authors($cat){
		$title = 'Authors';
		$category = Category::find($cat);
		$categoryAuthors = Category::find($cat)->authors;
		$route = Route::currentRouteName();
		
//		return $authors = Author::find(4)->categories;
		
		return view('authors', compact('category', 'title', 'categoryAuthors', 'route'));
	}
	
	public function papers(){
		$title = 'Paper';
		$paper = Paper::where('author', Input::get('author-id'))->where('category', Input::get('category-id'))->get();
		$category = Category::where('id', Input::get('category-id'))->get();
		
		$document = $category[0]->name.'/'.$paper[0]->title;
		$paperTitle = $paper[0]->title;
		
		return view('show', compact('title', 'document', 'paperTitle'));
	}
	
	public function delegates(){
		$title = 'Delegate List';
		$document = 'delegates';
		$paperTitle = 'List of Authors/Delegates';
		
		return view('show', compact('title', 'document', 'paperTitle'));
	}
	
	public function sponsors(){
		$title = 'Sponsors';
		return view('sponsors', compact('title'));
	}
}
