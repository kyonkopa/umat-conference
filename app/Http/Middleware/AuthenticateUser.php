<?php namespace App\Http\Middleware;
use App\User;
use Illuminate\Support\Facades\Cookie;
use Closure;
use Symfony\Component\HttpFoundation\Response;

class AuthenticateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // if(Cookie::get('ref')){
        //     return true;
        // }else{
        //     $returnCookie = User::verifyCredentials($request->input('ref'), $request->input('pin'));
        //     if(!$returnCookie){
        //         return redirect('/');
        //     }else{
        //         return Response::view('portal.index')->withCookie($returnCookie);
        //     }   
        // }
        return view('portal.index');
        // return User::verifyCredentials($request->input('ref'), $request->input('pin'));
    }
}
