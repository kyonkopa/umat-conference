<?php

Route::get('/', 'WelcomeController@index');
Route::get('programme', 'WelcomeController@programme');
Route::get('papers', 'WelcomeController@papers');
Route::get('delegates', 'WelcomeController@delegates');
Route::get('sponsors', 'WelcomeController@sponsors');
Route::post('search', 'ContentController@search');

Route::group(['prefix' => 'conference-papers'], function(){
     Route::get('/', 'WelcomeController@paperCategories');
     Route::get('{category}', 'WelcomeController@authors');
});

Route::group(['prefix' => 'content'], function(){
     Route::get('programme', 'ContentController@programme');
     Route::get('delegates', 'ContentController@delegates');
     Route::get('{category}/{title}', 'ContentController@paper');
});