<?php

namespace App;

// use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Hash;
use Illuminate\Support\Facades\Cookie;

class User extends Model
{
    protected $table = 'users';
    protected $primaryKey = 'Id';

//    protected $fillable = [
//        'name', 'email', 'password',
//    ]; 


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
//    protected $hidden = [
//        'password', 'remember_token',
//    ];
    
    public static function verifyCredentials($ref, $pin){
        $Student = self::whereRaw("ReferenceNo = '{$ref}' AND password = password('{$pin}')")->get();

        if(is_object($Student)){

            // Session::regenerate();
            // Session::put('ref', $ref);
            
            return Cookie::make('ref', $ref, 5);

        }else return false;
        
        // return self::where('ReferenceNo', '=', $ref)->get();
    }
}
