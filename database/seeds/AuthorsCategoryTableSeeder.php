<?php

use Illuminate\Database\Seeder;

class AuthorsCategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors_category')->insert([
            ['author_id' => '1', 'category_id' => '3'],
            ['author_id' => '4', 'category_id' => '1'],
            ['author_id' => '4', 'category_id' => '3'],
            ['author_id' => '5', 'category_id' => '2'],
            ['author_id' => '2', 'category_id' => '5'],
            ['author_id' => '7', 'category_id' => '1']
        ]);
    }
}
