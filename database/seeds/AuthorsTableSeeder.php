<?php

use Illuminate\Database\Seeder;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('authors')->insert([
            ['fullname' => 'Kwasi Yonkopa'],
            ['fullname' => 'AA Oteng Yeboah'],
            ['fullname' => 'MA Hirons']
        ]);
    }
}
