<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            ['name' => 'Environmental Issues in Mining'],
            ['name' => 'Geological and Geomatic Engineering'],
            ['name' => 'Corporate Social Responsibility, Sustainable Development and Small Scale Mining'],
            ['name' => 'Innovation in Mining and Petroleum Engineering'],
            ['name' => 'Advances in Miniral Extraction'],
            ['name' => 'Mathematics Electrical, Computing and Mechanical Applications']
        ]);
    }
}
