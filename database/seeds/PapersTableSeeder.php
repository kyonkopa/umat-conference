<?php

use Illuminate\Database\Seeder;

class PapersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('papers')->insert([
            ['title' => 'Effects ofAcid Sulphate Soils Redox Dynamics on Conventional
Borehole Measurement of Groundwater Quality', 'author' => '1', 'category' => '1'],
            ['title' => 'Mineral Development Infrastructure – A Green Community
Legacy?', 'author' => '2', 'category' => '1'],
            ['title' => 'A consideration of the critical environmental concerns in mining
and other extractive industries in Ghana', 'author' => '3', 'category' => '2']
        ]);
    }
}
