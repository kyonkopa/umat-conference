$(document).ready(function () {
     var Input = $('input[name=q]');

     Input.keyup(function () {
          $.ajax({
               url: 'search',
               type: 'post',
               data: {
                    q: Input.val()
               },
               success: function (response) {
                    $('.predictions').empty();

                    if (!$('.predictions').hasClass('show')) {
                         $('.predictions').slideDown(300);
                         $('.predictions').addClass('show');
                    }

                    [].forEach.call(response, function (obj) {
                         if (obj.title) {
                              var type = 'paper';
                         }

                         $("<a>", {
                              class: 'item ' + type,
                              text: obj.title.substr(0, 60)
                         }).append("<em>" + type + "</em>").attr('href', "papers?category-id=" + obj.category + "&author-id=" + obj.author).appendTo($('.predictions'));


                    });
               },
               error: function () {
                    console.log('oops net problem');
               }
          });
     });
});