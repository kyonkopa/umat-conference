@extends('layout.main')
@section('head')
<link rel="stylesheet" href="{{URL::to('css/lone.css')}}">
@endsection

@section('content')
     <p class="sub-head">Conference Papers > {{$category->name}}</p>
     <section class="grid">
          @foreach($categoryAuthors as $author)
          <a href="{{URL::to('papers')}}?category-id={{$category->id}}&author-id={{$author->id}}" class="select">{{$author->fullname}}</a>
          @endforeach
     </section>
@endsection