<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <title>UMaT Conference  | {{ $title }}</title>
    <link rel="stylesheet" href="{{URL::to('css/main.css')}}"/>
    <link rel="stylesheet" href="{{URL::to('fonts/font-awesome-4.4.0/css/font-awesome.min.css')}}"/>
    <link rel="icon" href="{{URL::to('img/favicon.png')}}"/>
    @yield('head')
</head>

<body>
    <header>
         University of Mines and Technology
                   <div class="img"></div>

    </header>
     <header>
          <p class="title"><a href="{{URL::to('/')}}">4th UMaT Biennial International Mining and Mineral Conference</a></p>
     </header>

     <section class="wrapper">
        @yield('content')
     </section>
     <footer>
          &copy;2016 University of Mines and Technology, Biennial Conferences
     </footer>
</body>

</html>