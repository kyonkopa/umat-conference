@extends('layout.main')

@section('head')
<link rel="stylesheet" href="{{URL::to('css/lone.css')}}">
@endsection

@section('content')

<p class="sub-head">Please select a category to view papers 
     <i class="fa fa-chevron-down"></i>
</p>

@foreach($categories as $category)
     <a href="conference-papers/{{$category->id}}" class="select category">{{$category->name}}</a>
@endforeach

@endsection