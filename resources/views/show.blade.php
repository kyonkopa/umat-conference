@extends('layout.main')
@section('head')
     <link rel="stylesheet" href="{{URL::to('css/lone.css')}}">
@endsection

@section('content')
    <p class="sub-head">{{$paperTitle}}</p>
     <iframe src="{{URL::to('content/'.$document)}}" frameborder="0"></iframe>
@endsection