@extends('layout.main')
@section('head')
     <link rel="stylesheet" href="{{URL::to('css/lone.css')}}">
@endsection

@section('content')
    <p class="sub-head">{{$title}}</p>
     <img class="image sponsors" src="{{URL::to('img/sponsors.jpg')}}"></img>
@endsection