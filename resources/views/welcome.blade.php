@extends('layout.main')


@section('content')
<form action="">
     <i class="fa fa-search"></i>
     <input type="text" name="q" placeholder="search here...">
     
     <div class="predictions">
          <span class="item">Item 1</span>
          <span class="item">Item 2</span>
          <span class="item">Item 3</span>
     </div>
</form>

<div class="grid">
     <a class="grid-item" href="{{URL::to('programme')}}">
          <i class="icon fa fa-suitcase"></i>
          <div class="detail">
               <span>Programme</span>
               <em class="desc">What's the programme about</em>
          </div>
     </a>
     
     <a class="grid-item" href="{{URL::to('conference-papers')}}">
          <i class="icon fa fa-file"></i>
          <div class="detail">
               <span>Conference Papers</span>
               <em class="desc">What's the programme about</em>
          </div>
     </a>  
     
     <a class="grid-item" href="{{URL::to('delegates')}}">
          <i class="icon fa fa-users"></i>
          <div class="detail">
               <span>Delegates</span>
               <em class="desc">What's the programme about</em>
          </div>
     </a>   
     
     <a class="grid-item" href="{{URL::to('sponsors')}}">
          <i class="icon fa fa-thumbs-o-up"></i>
          <div class="detail">
               <span>Sponsors</span>
               <em class="desc">What's the programme about</em>
          </div>
     </a>
</div>

<button class="exit fa fa-close"></button>
<script src="{{URL::to('js/jquery.js')}}"></script>
<script src="{{URL::to('js/script.js')}}"></script>
@endsection